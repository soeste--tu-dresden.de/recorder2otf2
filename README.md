# Recorder to OTF2
simple script for converting recorder traces to otf2 traces

## Usage
```python3 recorder_to_otf2.py <file> <args>```

###args
| arg | function |
|-----| --- |
| -o  | sets output path |
| -t  | sets timer resolution to use (default is 1e9)|